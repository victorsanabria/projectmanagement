const usersService = require('../services/users')

const login = async (req, res, next) => {
  try {
    const token = await usersService.login(req.body)
    res.status(200).json({
      msg: 'Logged in successfully. Data in token payload',
      data: { ok: true, token }
    })
  } catch (error) {
      next(error)
  }
}

module.exports = {
    login,
}