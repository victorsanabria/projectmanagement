const projectsService = require('../services/projects')

const create = async (req, res, next) => {
    try {
      const projectCreated = await projectsService.create(req.body)
      res.status(201).json({ msg: 'Project created succesfully', data: projectCreated })
    } catch (error) {
      next(error)
    }
}

const getById = async (req, res, next) => {
  try {
    const project = await projectsService.getById(req.params.id)
    res.status(200).json({ data: project })
  } catch (error) {
    next(error)
  }
}

const getByName = async (req, res, next) => {
  try {
    const project = await projectsService.getByName(req.query.name)
    res.status(200).json({ data: project })
  } catch (error) {
    next(error)
  }
}

const getAll = async (req, res, next) => {
  try {
    
    const resObj = await projectsService.getAll(req)
    
    res.status(200).json(resObj)
  } catch (error) {
    next(error)
  }
}

const remove = async (req, res, next) => {
  try {
    await projectsService.remove(req.params.id)
    res.status(200).json({ msg: 'Project removed succesfully' })
  } catch (error) {
    next(error)
  }
}

const update = async (req, res, next) => {
  try {
    const projectUpdated = await projectsService.update(req.params.id, req.body)
    res.status(200).json({ msg: 'Project updated succesfully', data: projectUpdated })
  } catch (error) {
    next(error)
  }
}

const assignUser = async (req, res, next) => {
  try {
    const userUpdated = await projectsService.assignUser(req.body.assignedUser, req.body.projectId)
    res.status(200).json({ msg: 'Assign project user updated succesfully', data: userUpdated })
  } catch (error) {
    next(error)
  }

}

module.exports = {
  create,
  getById,
  getByName,
  getAll,
  remove,
  update,
  assignUser,
  
}