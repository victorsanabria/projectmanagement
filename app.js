var express = require('express');
var bodyParser = require('body-parser');
const indexRouter = require('./routes/index')

var app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/', indexRouter);

app.get('/', function(req, res){
	res.status(200).send({
		message: 'GET Home route working fine!'
	});
});

app.listen(process.env.PORT || 3000, function(){
	console.log(`Listening at port ${process.env.PORT}`);

});