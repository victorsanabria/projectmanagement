const { body } = require('express-validator')
const { executeValidation } = require('./validation-index')

const email = body('email')
  .exists().withMessage('param required').bail()
  .isEmail().withMessage('must be a valid email').bail()
  .normalizeEmail()

const password = body('password')
  .exists().withMessage('param required').bail()
  .isString().withMessage('must be a string').bail()
  .notEmpty().withMessage('cannot be empty').bail()


const login = [email, password, executeValidation]
  
module.exports = {
  login
    
}
  