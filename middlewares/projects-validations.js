const { body, param, query } = require('express-validator')
const { executeValidation } = require('./validation-index')

const name = body('name')
  .exists().withMessage('param required').bail()
  .isString().withMessage('must be a string').bail()
  .notEmpty().withMessage('cannot be empty').bail()

const description = body('description')
  .exists().withMessage('param required').bail()
  .isString().withMessage('must be a string').bail()
  .notEmpty().withMessage('cannot be empty').bail()


const managerId = body('managerId')
  .exists().withMessage('param required').bail()
  .isInt().withMessage('must be an integer').bail()


const status = body('status')
  .exists().withMessage('param required').bail()
  .isString().withMessage('must be a String').bail()
  .isIn(['Enabled', 'Disabled']).withMessage('Wrong status value').bail()


const idParam = param('id')
  .exists().withMessage('param required').bail()
  .isInt().withMessage('must be an integer').bail()


const assignedUser = body('assignedUser')
  .isInt().withMessage('must be an integer').bail()
  .optional()
  .bail()


const projectId = body('projectId')
  .isInt().withMessage('must be an integer').bail()
  .optional()


const nameUpdate = body('name')
  .notEmpty().optional().withMessage('cannot be empty').bail()
  .isString().optional().withMessage('must be a string').bail()
  

const descriptionUpdate = body('description')
  .notEmpty().optional().withMessage('cannot be empty').bail()
  .isString().optional().withMessage('must be a string').bail()


const managerIdUpdate = body('managerId')
  .notEmpty().optional().withMessage('cannot be empty').bail()
  .isInt().optional().withMessage('must be an integer').bail()


const statusUpdate = body('status')
  .notEmpty().optional().withMessage('cannot be empty').bail()
  .isString().optional().withMessage('must be a String').bail()
  .isIn(['Enabled', 'Disabled']).withMessage('Wrong status value').bail()

const getName = query('name')
  .exists().withMessage('param required').bail()
  .isString().withMessage('must be a string').bail()
  .notEmpty().withMessage('cannot be empty').bail()



const create = [name, description, managerId, status, assignedUser, executeValidation]

const remove = [idParam, executeValidation]

const getById = [idParam, executeValidation]

const update = [idParam, nameUpdate, descriptionUpdate, managerIdUpdate, statusUpdate, assignedUser, executeValidation]

const getByName = [getName, executeValidation]

const assignUser = [assignedUser, projectId, executeValidation]

module.exports = {
  create,
  remove,
  getById,
  getByName,
  update,
  assignUser

}
