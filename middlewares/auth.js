const usersRepository = require('../repositories/users')
const { verifyToken } = require('../modules/auth')
const rolesRepository = require('../repositories/roles')
const { adminRoleName } = require('../config/config')

const getTokenPayload = (req) => {
  const authToken = req.headers.authorization
  const token = authToken && authToken.startsWith('Bearer ') && authToken.split(' ')[1]
  if (!token) {
    const error = new Error('Please provided a token Bearer in authorization')
    error.status = 401
    throw error
  }
  return verifyToken(token)
}

const isAdmin = async (req, res, next) => {
  try {
    const token = getTokenPayload(req)
    const role = await usersRepository.getRoleById(token.userId)
    const roleUser = await rolesRepository.getRoleById(role.roleId)

    if (roleUser.name !== adminRoleName) {
      const error = new Error('Role admin required')
      error.status = 403
      throw error
    }
    next()
  } catch (error) {
    next(error)
  }
}

module.exports = {
  isAdmin,
  getTokenPayload
}

