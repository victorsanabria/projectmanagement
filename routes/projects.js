const express = require('express')
const router = express.Router()
const authMidd = require('../middlewares/auth')
const validationsMidd = require ('../middlewares/projects-validations')

const projectsController = require('../controllers/projects')

router.post('/', authMidd.isAdmin, validationsMidd.create, projectsController.create)
router.delete('/:id', authMidd.isAdmin, validationsMidd.remove, projectsController.remove)
router.patch('/:id', authMidd.isAdmin, validationsMidd.update, projectsController.update)
router.put('/assignUser', authMidd.isAdmin, validationsMidd.assignUser, projectsController.assignUser)
router.get('/Id/:id', validationsMidd.getById, projectsController.getById)
router.get('/name', validationsMidd.getByName, projectsController.getByName)
router.get('/', projectsController.getAll)


module.exports = router
