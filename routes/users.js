const express = require('express')
const router = express.Router()
const validationsMidd = require ('../middlewares/users-validations')


const usersController = require('../controllers/users')

router.post('/login',validationsMidd.login , usersController.login)


module.exports = router
