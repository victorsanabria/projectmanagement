const express = require('express')
const router = express.Router()
const swaggerUi = require('swagger-ui-express')

const projectsRouter = require('./projects')
const usersRouter = require('./users')
const docs = require('../docs')

router.use('/api/docs', swaggerUi.serve)
router.get('/api/docs', swaggerUi.setup(docs))
router.use('/projects', projectsRouter)
router.use('/users', usersRouter)

module.exports = router