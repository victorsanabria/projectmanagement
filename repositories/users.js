const db = require('../models')

const updateAssignedUser = async (assignedUser, project_id) => {
    await db.Users.update({ project_id: project_id }, { where: { id: assignedUser } }) 
}

const getById = async (id) => {
  const project = await db.Users.findByPk(id, {attributes: ['id', 'email','project_id', 'createdAt', 'updatedAt']} )
  return project
}

const getRoleById = async (id) => {
  const project = await db.Users.findByPk(id, {attributes: ['roleId']} )
  return project

}

const getByEmail = async (userEmail) => {
  console.log(userEmail)
  const data = await db.Users.findOne( {where: { email: userEmail } 
  })
  return data
}


module.exports = {
    getById,
    getByEmail,
    getRoleById,
    updateAssignedUser
}