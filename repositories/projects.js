const db = require('../models')

const create = async (project) => {
  const projectCreated = await db.Projects.create(project)
  console.log(projectCreated)
  return projectCreated
  

}

const getById = async (id) => {
  const project = await db.Projects.findByPk(id, { include: [{ model:db.Users,
    attributes: ['id', 'email','project_id', 'createdAt', 'updatedAt'] }] })
  return project
}

const getByName = async (name) => {
  const project = await db.Projects.findAll( {where: { name: name }  } )
  
  return project
}

const getAll = async (offset, limit) => {
  const countAndRows = await db.Projects.findAndCountAll({  
    attributes: ['id', 'name', 'description', 'status', 'managerId'],
    include: [{ model:db.Users, attributes: ['id', 'email','project_id', 'createdAt', 'updatedAt'] }],
    order: [['id', 'ASC']],
    offset,
    limit
  })
  return countAndRows
}

const remove = async (id) => {
  const rowsRemovedCount = await db.Projects.destroy({ where: { id: id } })
  return rowsRemovedCount

}

const update = async (id, project) => {
  const [rowsUpdatedCount] = await db.Projects.update(project, { where: { id: id } })
  return rowsUpdatedCount
} 


module.exports = {
    create,
    getById,
    getByName,
    getAll,
    remove,
    update

}