module.exports = {
    responses: {
      Removed: {
        description: 'Resource removed',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                msg: {
                  type: 'string',
                  example: 'Entity removed succesfully'
                }
              }
            }
          }
        }
      },
      ValidationError: {
        description: 'Validation Error',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/ValidationError'
            }
          }
        }
      },
      Unauthorized: {
        description: 'Missing or invalid token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                error: {
                  type: 'string',
                  example: 'Bearer Token Invalid'
                }
              }
            }
          }
        }
      },
      Forbidden: {
        description: 'Forbidden resource',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                error: {
                  type: 'string',
                  example: 'Role admin required'
                }
              }
            }
          }
        }
      },
      NotFound: {
        description: 'Resource Not Found',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                error: {
                  type: 'string',
                  description: '',
                  example: 'Resource with id 1 not found'
                }
              }
            }
          }
        }
      }
    }
  }
  