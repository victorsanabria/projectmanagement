const responses = require('./responses')

module.exports = {
  components: {
    securitySchemes: {
      bearerAuth: {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT'
      }
    },
    schemas: {
      Id: {
        type: 'number',
        description: 'ID',
        example: '1'
      },
      ValidationError: {
        type: 'object',
        properties: {
          errors: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                value: {
                  description: 'value provided',
                  example: 4
                },
                msg: {
                  type: 'string',
                  example: 'must be a string'
                },
                param: {
                  type: 'string',
                  example: 'name'
                },
                location: {
                  type: 'string',
                  example: 'body'
                }
              }
            }
          }
        }
      },
    },
    ...responses
  }
}
