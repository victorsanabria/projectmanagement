const projectsSchemas = require('./schemas')

module.exports = {
  get: {
    security: [{ bearerAuth: [] }],
    tags: ['Projects'],
    description: 'Get one project by name',
    operationId: 'getByName',
    parameters: [
      {
        name: 'name',
        in: 'query',
        schema: {
          type: 'string',
          example: 'Project 1'
        },
        description: 'Name of the project'
      }
    ],
    responses: {
      200: {
        description: 'Get detail of selected project',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                data: projectsSchemas.projectGetName
              }
            }
          }
        }
      },
      400: {
        $ref: '#/components/responses/ValidationError'
      },
      401: {
        $ref: '#/components/responses/Unauthorized'
      },
      404: {
        $ref: '#/components/responses/NotFound'
      }
    }
  }
}
