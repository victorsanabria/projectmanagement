const projectsSchemas = require('./schemas')

module.exports = {
  patch: {
    security: [{ bearerAuth: [] }],
    tags: ['Projects'],
    description: 'Update a Project',
    operationId: 'update',
    parameters: [
      {
        name: 'id',
        in: 'path',
        schema: {
          $ref: '#/components/schemas/Id'
        },
        required: true,
        description: 'Entity Id'
      }
    ],
    requestBody: {
      content: {
        'application/json': {
          schema: projectsSchemas.projectUpdate
        }
      }
    },
    responses: {
      200: {
        description: 'Detail of updated project',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                msg: {
                  type: 'string',
                  example: 'Entity updated succesfully'
                },
                data: projectsSchemas.projectGetById
              }
            }
          }
        }
      },
      400: {
        $ref: '#/components/responses/ValidationError'
      },
      401: {
        $ref: '#/components/responses/Unauthorized'
      },
      403: {
        $ref: '#/components/responses/Forbidden'
      },
      404: {
        $ref: '#/components/responses/NotFound'
      }
    }
  }
}
