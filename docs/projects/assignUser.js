const projectsSchemas = require('./schemas')

module.exports = {
  put: {
    security: [{ bearerAuth: [] }],
    tags: ['Projects'],
    description: 'Assign project to user, or unassign project sending only assignedUser',
    operationId: 'assign projectId',
    requestBody: {
      content: {
        'application/json': {
          schema: projectsSchemas.projectAssignUser
        },
      }
    },
    responses: {
      200: {
        description: 'Detail of updated user',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                msg: {
                  type: 'string',
                  example: 'Assign project user updated succesfully'
                },
                data: projectsSchemas.projectUser
              }
            }
          }
        }
      },
      400: {
        $ref: '#/components/responses/ValidationError'
      },
      401: {
        $ref: '#/components/responses/Unauthorized'
      },
      403: {
        $ref: '#/components/responses/Forbidden'
      },
      404: {
        $ref: '#/components/responses/NotFound'
      },
    }
  }
}
