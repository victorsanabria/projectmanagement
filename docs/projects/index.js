const create = require('./create')
const update = require('./update')
const remove = require('./remove')
const getAll = require('./getAll')
const getById = require('./getById')
const getByName = require('./getByName')
const assignUser = require('./assignUser')

module.exports = {
  '/projects': {
    ...create,
    ...getAll,
  },
  '/projects/{id}': {
    ...update,
    ...remove,
  },
  '/projects/Id/{id}': {
    ...getById
  },
  '/projects/name': {
    ...getByName
  },
  '/projects/assignUser': {
    ...assignUser
  }

}
