const projectsSchemas = require('./schemas')

module.exports = {
  get: {
    security: [{ bearerAuth: [] }],
    tags: ['Projects'],
    description: 'Get list of projects paginated',
    operationId: 'getAll',
    parameters: [
      {
        name: 'page',
        in: 'query',
        schema: {
          type: 'number',
          example: 1
        },
        description: 'Number of the page'
      }
    ],
    responses: {
      200: {
        description: 'List of projects with pagination data',
        content: {
          'application/json': {
            schema: projectsSchemas.resProjectGetAll
          }
        }
      },
      400: {
        $ref: '#/components/responses/ValidationError'
      },
      401: {
        $ref: '#/components/responses/Unauthorized'
      },
      403: {
        $ref: '#/components/responses/Forbidden'
      },    
    }
  }
}
