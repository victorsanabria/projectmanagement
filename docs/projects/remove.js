
module.exports = {
    delete: {
      security: [{ bearerAuth: [] }],
      tags: ['Projects'],
      description: 'Remove one project',
      operationId: 'remove',
      parameters: [
        {
          name: 'id',
          in: 'path',
          schema: {
            $ref: '#/components/schemas/Id'
          },
          required: true,
          description: 'Entity Id'
        }
      ],
      responses: {
        200: {
          $ref: '#/components/responses/Removed'
        },
        400: {
          $ref: '#/components/responses/ValidationError'
        },
        401: {
          $ref: '#/components/responses/Unauthorized'
        },
        403: {
          $ref: '#/components/responses/Forbidden'
        },
        404: {
          $ref: '#/components/responses/NotFound'
        },
      }
    }
  }
  