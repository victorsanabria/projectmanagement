const projectSchemas = require('./schemas')

module.exports = {
  post: {
    security: [{ bearerAuth: [] }],
    tags: ['Projects'],
    description: 'Create a Project.',
    operationId: 'create',
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: projectSchemas.projectsCreate
        }
      }
    },
    responses: {
      201: {
        description: 'Detail of created project ',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                msg: {
                  type: 'string',
                  example: 'Project created successfully'
                },
                data: projectSchemas.projectGetById
              }
            }
          }
        }
      },
      400: {
        $ref: '#/components/responses/ValidationError'
      },
      401: {
        $ref: '#/components/responses/Unauthorized'
      },
      403: {
        $ref: '#/components/responses/Forbidden'
      },
      404: {
        $ref: '#/components/responses/NotFound'
      },
    }
  }
}
