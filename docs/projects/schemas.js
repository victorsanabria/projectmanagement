const id = {
    $ref: '#/components/schemas/Id'
}
const name = {
    type: 'string',
    description: 'project name',
    example: 'Landing page'
}
const description = {
    type: 'string',
    description: 'project description',
    example: 'Description of landing page'
}
const managerId = {
    type: 'Integer',
    description: 'manager Id to assign a new project',
    example:2
}
const status = {
    type: 'string',
    description: 'project status',
    example: 'Enabled',
}
const assignedUser = {
    type: 'Integer',
    description: 'user Id to assign a new project',
    example:4
}
const projectId= {
  type:'Integer',
  description:'project id',
  example:3
}
const email= {
  type:'String',
  description:'email user',
  example:'victor-sanabria@test.com'
}
const Users = {
  type: 'object',
  properties: {
    id,
    email,
    projectId,
  }
}
  
module.exports = {
  resProjectGetAll: {
    type: 'object',
    properties: {
      pagesUrl: {
        type: 'object',
        properties: {
          previous: {
            oneOf: [
              {
                type: 'string',
                description: 'url to previous page',
                example: 'http://localhost:3000/projects/?page=1'
              },
              {
                type: 'null',
                description: 'previous page does not exist',
                example: null
              }
            ]
          },
          next: {
            oneOf: [
              {
                type: 'null',
                description: 'next page does not exist',
                example: null
              },
              {
                type: 'string',
                description: 'url to next page',
                example: 'http://localhost:3000/projects/?page=3'
              }
            ]
          }
        }
      },
      itemsCount: {
        type: 'number',
        description: 'count of entities in database',
        example: 15
      },
      totalPages: {
        type: 'number',
        description: 'count of available pages',
        example: 2
      },
      data: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id,
            name,
            description,
            status,
            managerId,
            Users
          }
        }
      }
    }
  },
  projectGetById: {
    type: 'object',
    properties: {
      id,
      name,
      description,
      managerId,
      status,
      Users
    }
  },
  projectGetName: {
    type: 'object',
    properties: {
      id,
      name,
      description,
      managerId,
      status,
    }
  },
  projectGetByName: {
    type: 'object',
    properties: {
      name

    }
  },
  projectAssignUser: {
    type: 'object',
    properties: {
      assignedUser,
      projectId
    }
  },
  projectUser: {
    type: 'object',
    properties: {
      Users
    }
  },
  projectsCreate: {
    type: 'object',
    properties: {
      name,
      description,
      managerId,
      status,
      assignedUser
    }
  },
  projectUpdate: {
    type: 'object',
    properties: {
      name,
      description,
      managerId,
      status,
      Users,
      assignedUser
    }
  },

}
  