module.exports = {
    Users: {
      type: 'object',
      properties: {
        Name: {
          type: 'string',
          description: 'The user name',
          example: 'Victor Sanabria'
        },
        email: {
          type: 'string',
          description: 'The user email',
          example: 'victor-sanabria@test.com'
        },
        project_id: {
          type: 'string',
          description: 'Id of assigned project',
          example: '2'
        }
      }
    },
    LoggedIn: {
      type: 'object',
      properties: {
        msg: {
          type: 'string',
          description: 'The message of response',
          example: 'Logged in successfully. Data in token payload'
        },
        data: {
          type: 'object',
          properties: {
            ok: {
              type: 'string',
              description: 'The user email',
              example: 'true'
            },
            token: {
              type: 'string',
              description: 'The user token',
              example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTY0NDE4MDczMywiZXhwIjoxNjQ0MjY3MTMzfQ.1eNk3cjmvkat7z_NsgStX7Wm2pXKoANSliuhTFwSdVM'
            }
          }
        }
      }
    },
    Login: {
      type: 'object',
      properties: {
        email: {
          type: 'string',
          description: 'The user email',
          example: 'admin-test-user@test.com'
        },
        password: {
          type: 'string',
          description: 'The user password',
          example: 'TestPassword123!'
        }
      }
    },
}