const tags = require('./tags')
const basicInfo = require('./basicInfo')
const components = require('./components')
const projects = require('./projects')
const servers = require('./servers')
const users = require('./users')


module.exports = {
  ...basicInfo,
  ...servers,
  ...tags,
  ...components,
  paths: {
    ...projects,
    ...users

  }
}
