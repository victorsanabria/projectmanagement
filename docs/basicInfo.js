module.exports = {
    openapi: '3.0.3',
  
    info: {
      title: ' API RESTFUL FOR MANAGER PROJECTS',
      version: '1.0.0',
  
      description: 'A CRUD API with Express and Sequelize and documented with Swagger',
  
      contact: {
        name: 'Victor Sanabria',
        email: 'viktor10511051@gmail.com'
      }
  
    }
  
  }