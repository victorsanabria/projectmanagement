# Server Base - Project Manager

Creation of REST API for project management, made in NodeJS – Express, mySQL Database – Sequelize, authentication, JWT, bcrypt, and deployed on heroku.

In the heroku database we will have the following tables available with their rows.

## Roles table

| ID | Name | Description |
| ------------- | ------------- | ------------- |
| 1  | Standard  | Usuario Standard |
| 2  | Admin  |	Usuario Administrador	|


## Projects table

| ID | Name | description | managerId | Status |
| ------------- | ------------- | ------------- | ----------- | ----------- |
| 1  | Project 1  | Description project 1 | 1 |	Enabled |
| 2  | Project 2  | Description project 2 | 1 | Enabled |
| 3  | Project 3  | Description project 3 | 2 | Enabled |
| 4  | Project 4  | Description project 4 | 3 | Enabled |


## Users table

Generates 6 standard account users and 2 administrator account users with the credentials:

### Standard Users

| Email | Password | Name | RoleId |
| ------------- | ------------- | ------------- | ----------- |
| standard-test-@test.com  | TestPassword123!  | Standard | 1 |
| marcos-perez@test.com  | TestPassword123!  | Marcos Perez | 1 |
| jose-perez@test.com  | TestPassword123!  | Jose Perez | 1 |
| mauricio-perez@test.com  | TestPassword123!  | Mauricio Perez | 1 |
| juan-ignacio@test.com  | TestPassword123!  | Juan Ignacio | 1 |
| francisco-ignacio@test.com  | TestPassword123!  | Francisco Ignacio | 1 |

### Administrators Users

| Email | Password | Name | RoleId |
| ------------- | ------------- | ------------- | ----------- |
| admin-test-user@test.com  | TestPassword123!  | Admin | 2 |
| victor-sanabria@test.com  | TestPassword123!  | Victor Sanabria | 2 |




# Heroku deploy

https://project-management-estoes.herokuapp.com/



## Project documentation URL 

https://project-management-estoes.herokuapp.com/api/docs/





