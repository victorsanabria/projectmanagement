'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', [{
      email: 'standard-test-@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Test',
      roleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      email: 'admin-test-user@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Admin',
      roleId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      email: 'victor-sanabria@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Victor Sanabria',
      roleId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      email: 'marcos-perez@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Marcos Perez',
      roleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      email: 'jose-perez@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Jose Perez',
      roleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      email: 'mauricio-perez@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Mauricio Perez',
      roleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      email: 'juan-ignacio@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Juan Ignacio',
      roleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      email: 'francisco-ignacio@test.com',
      password: '$2a$12$pnRk4pz/CjzV10l.p0RkoujnS1Z104wq3ILF8dKY.SioVC0f1qnzy', // TestPassword123!
      name:'Francisco Ignacio',
      roleId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], 
    {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {})
  }
}
