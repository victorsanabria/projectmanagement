'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Projects', [{
      name: 'Project 1',
      description: 'Description project 1',
      managerId: 1,
      status: 'Enabled',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Project 2',
      description: 'Description project 2',
      managerId: 1,
      status: 'Enabled',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Project 3',
      description: 'Description project 3',
      managerId: 2,
      status: 'Enabled',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Project 4',
      description: 'Description project 4',
      managerId: 3,
      status: 'Enabled',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Projects', null, {})
  }
}
