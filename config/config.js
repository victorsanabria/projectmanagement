require('dotenv').config()

module.exports = {
  adminRoleName: 'Admin',
  standardRoleName: 'Standard',
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    Port: process.env.DB_PORT,
    URL: process.env.DATABASE_URL,
    dialect: 'mysql'
  },
  test: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    URL: process.env.DATABASE_URL,
    dialect: 'mysql'
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    Port: process.env.DB_PORT,
    URL: process.env.DATABASE_URL,
    dialect: 'mysql'
  },
  token: {
    secret: process.env.JWT_PASS,
    expiresIn: '1d'
  },
}