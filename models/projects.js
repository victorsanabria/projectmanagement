const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class Projects extends Model {
    /**
      * Helper method for defining associations.
      * This method is not a part of Sequelize lifecycle.
      * The `models/index` file will call this method automatically.
    */
    static associate (models) {
      Projects.hasMany(models.Users, {
        foreignKey: 'project_id'
      })        
    }
  }

  Projects.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    managerId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false
    },

  }, {
    sequelize,
    modelName: 'Projects',
    deletedAt: 'deletedAt',
    paranoid: true,
    timestamps: true
  })
  return Projects
}
