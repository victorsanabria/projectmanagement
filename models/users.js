'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      Users.belongsTo(models.Roles, { as: 'role' })
      Users.belongsTo(models.Projects, { foreignKey:'id',
      target_key: 'project_id'
      })
    }
  }
  Users.init(
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      roleId: DataTypes.INTEGER,
      deletedAt: DataTypes.DATE
    },
    {
      sequelize,
      modelName: 'Users',
      timestamps: true,
      paranoid: true

    }
  )

  return Users
}