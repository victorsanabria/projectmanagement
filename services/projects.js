const projectsRepository = require('../repositories/projects')
const usersRepository = require('../repositories/users')
const { paginate } = require('../modules/pagination')

const remove = async (id) => {
  const currentProject = await projectsRepository.getById(id)
  if (currentProject === null) {
    const error = new Error(`Project with id ${id} not found`)
    error.status = 404
    throw error
  }
  await projectsRepository.remove(id)
}

const create = async (projectToCreate) => {
  const userSelected = await usersRepository.getById(projectToCreate.managerId)

  if (userSelected === null) {
    const error = new Error(`ManagerId user with id ${projectToCreate.managerId} not found.`)
      error.status = 400
    throw error
  }

  const projectCreated = await projectsRepository.create(projectToCreate)
  if (projectToCreate.assignedUser) {
    const userSelected = await usersRepository.getById(projectToCreate.assignedUser)
    if (userSelected === null) {
      const error = new Error(`AssignedUser with id ${projectToCreate.assignedUser} not found.`)
      error.status = 400
      throw error
    } 
    await usersRepository.updateAssignedUser(projectToCreate.assignedUser, projectCreated.id)
  }

  return projectCreated
}

const getById = async (id) => {
  const currentProject = await projectsRepository.getById(id)
  if (currentProject === null) {
    const error = new Error(`Project with id ${id} not found`)
    error.status = 404
    throw error
  }
  return currentProject

} 

const getByName = async (name) => {
  const project = await projectsRepository.getByName(name)
  if (project.length == 0) {
    const error = new Error(`Project with name ${name} not found`)
    error.status = 404
    throw error
  }
  return project
}

const getAll = async (req) => {
  const resObj = paginate(projectsRepository.getAll, req, 3)
  return resObj
}

const update = async (id, projectToUpdate) => {
  if (projectToUpdate.managerId) {
    const userSelected = await usersRepository.getById(projectToUpdate.managerId)
    if (userSelected === null) {
      const error = new Error(`ManagerId user with id ${projectToUpdate.managerId} not found.`)
      error.status = 400
      throw error
    }
  }

  const currentProject = await projectsRepository.getById(id)
  if (currentProject === null) {
    const error = new Error(`Project with id ${id} not found`)
    error.status = 404
    throw error
  }

  if (projectToUpdate.assignedUser) {
    const userSelected = await usersRepository.getById(projectToUpdate.assignedUser)
    
    if (userSelected === null) {
      const error = new Error(`AssignedUser with id ${projectToUpdate.assignedUser} not found.`)
      error.status = 400
      throw error
    } 
    
    await usersRepository.updateAssignedUser(projectToUpdate.assignedUser, id )
  }

  await projectsRepository.update(id, projectToUpdate)
  const projectUpdated = await projectsRepository.getById(id)
  return projectUpdated
}

const assignUser = async (assignedUser, project_id = null) => {
  const userSelected = await usersRepository.getById(assignedUser)
    
  if (userSelected === null) {
    const error = new Error(`AssignedUser with id ${assignedUser} not found.`)
    error.status = 400
    throw error
  } 
    
  await usersRepository.updateAssignedUser(assignedUser, project_id)
  const userUpdated = await usersRepository.getById(assignedUser)
  return userUpdated
  
}


module.exports = {
  create,
  getById,
  getByName,
  getAll,
  remove,
  update,
  assignUser

}